��          �      �       0     1  '   7     _     h     t  �   {     (  1   C  
   u     �     �  *   �  �  �     �  J   �     �       
     I  (  t   r  V   �     >     R     h  w   l                               	             
              Alias Block contents to render in a template. Contents Description Hidden Page URL this block is related to. Regular expressions supported (e.g.: "/news.*" &#8212; everything under "/news").<br /><b>Reserved URL alias:</b> "*" &#8212; every page. Short memo for this block. Short name to address this block from a template. Site Block Site Blocks URL Whether to show this block when requested. Project-Id-Version: SiteBlocks
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-06-08 18:03+0700
PO-Revision-Date: 2010-06-08 18:14+0700
Last-Translator: Igor 'idle sign' Starikov <idlesign@yandex.ru>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Poedit-Language: Russian
X-Poedit-SourceCharset: utf-8
 Псевдоним Содержимое блока для вывода на странице. Содержимое Описание Скрыт URL для которого используется блок. Поддерживаются регулярные выражения (н.п.: "/news.*" &#8212; всё, что под адресом "/news").<br /><b>Зарезервированный URL-псевдоним:</b> "*" &#8212; блок для всех страниц. Справочная информация о блоке (чтобы не забыть, что это за блок). Короткое имя для обращения к блоку из шаблонов. Блок сайта Блоки сайта URL Определяет, следует ли отображать этот блок в случае его запроса. 